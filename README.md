# Snippets for the VueJs Coding Guidelines

Here is a bunch of snippets that could be used to work inside our spa to respect our coding guidelines that you can find on our [Confluence page](https://loud-project.atlassian.net/wiki/spaces/LOUD/pages/859111468/3.2.5.7.1+Vuejs).

## Download and Install

First you can download this snippet through git:

```shell
git clone https://maxgasnier@bitbucket.org/maxgasnier/fty-spa-guidelines-snippets.git
```

To install the snippet, go inside the repository and find the file named:
`spa-snippets.code-snippets`
Next you can copy-paste the file in the `.vscode` folder in your project.

⚠️ Remark: you can change the name, just keep the extension `.code-snippets`

## List of snippets

Here is a list of snippets you can use in your VS code files.

| Command       | Description                                              | Docs |
|---------------|----------------------------------------------------------|------|
| gen-fty-a     | Generate an atom component with project prefix           |      |
| gen-fty-m     | Generate a molecule component with project prefix        |      |
| gen-fty-o     | Generate an organism component with project prefix       |      |
| gen-fty-p     | Generate a page component with project prefix            |      |
| gen-fty-l     | Generate a layout component with project prefix          |      |
| gen-fty-data  | Generate data object inside the object of the component  |      |
| gen-fty-props | Generate props object inside the object of the component |      |
| fty-add-cptd  | Add computed function                                    |      |
| fty-cnt-err   | Will use the log system for contribution errors          |      |
| fty-app-err   | Will use the log system for app errors                   |      |

Documentation details:

### Generate Atom Components
- Command:
```html
gen-fty-a
```

- Example:
![gen-fty-a example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/gen-fty-a.gif)

### Generate Molecules components
- Command:
```html
gen-fty-m
```
- Example:
![gen-fty-a example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/gen-fty-m.gif)

### Generate Organisms components
- Command:
```html
gen-fty-o
```

- Example:
![gen-fty-o example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/gen-fty-o.gif)

### Generate Pages components
- Command:
```html
gen-fty-p
```

- Example:
![gen-fty-p example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/gen-fty-p.gif)

### Generate Layout components
- Command:
```html
gen-fty-l
```

- Example:
![gen-fty-l example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/gen-fty-l.gif)

### Generate data
- Command:
```html
gen-fty-data
```

- Example:
![gen-fty-data example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/gen-fty-data.gif)

### Generate Props
- Command:
```html
gen-fty-props
```

- Example:
![gen-fty-props example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/gen-fty-props.gif)

### Add Computed function
- Command:
```html
fty-add-cptd
```

- Example:
![fty-add-cptd example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/fty-add-cptd.gif)

### Will log as an app error handler
- Command:
```html
fty-app-err
```

- Example:
![fty-add-cptd example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/fty-app-err.gif)

### Will log as a contribution error handler
- Command:
```html
fty-cnt-err
```

- Example:
![fty-add-cptd example](https://bitbucket.org/maxgasnier/fty-spa-guidelines-snippets/raw/master/assets/fty-cnt-err.gif)